<?php

function recover_tid_for_taxonomy_name($name, $vid, $vocabulary) {
    $term = current(taxonomy_get_term_by_name($name, $vocabulary));
    if($term != FALSE){
      $output = $term->tid;
    }else{
      $data = array(
            'name' => $name,
            'vid' => $vid,
        );
      $e = entity_create('taxonomy_term', $data);
      $wrapper = entity_metadata_wrapper('taxonomy_term', $e );
      $wrapper->save();
      
      $output = $wrapper->tid->value();
    }

    return $output;
  }

 /*
 * 
 */
function uri_check($uri) {
  $sql = "SELECT uri FROM {file_managed}";
  $results = db_query($sql);
  
  foreach ($results as $row) {
    if ($row->uri == $uri) {
      $valor = FALSE;
    }else{
      $valor = TRUE;
    }
  }
  
  return $valor;
}

